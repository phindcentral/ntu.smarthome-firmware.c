#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
#include "util.h"
#include "debug.h"

#define BUF_SZ 1024

const char* TAG = "COMMAND_SENDER";

void connectServer(struct mosquitto *mosq, const char* host, int port) {
    while (mosquitto_connect(mosq, host, port, 600) != MOSQ_ERR_SUCCESS) {
        debug(TAG, "Connect Fail to %s:%d", host, port);
        sleep(2);
    }
    debug(TAG, "Connect Success to %s:%d", host, port);
}

void sendMessageToTopic(struct mosquitto* mosq, const char* topic, char* message) {
    debug(TAG, "Sending [%s]", trim(message));
    mosquitto_publish(mosq, NULL, topic, strlen(message), message, 0, 0);
}

bool prepareTopic(char* topic, int sz) {
    FILE* f = fopen("topic.config", "r");
    if (!f) return false;
    topic = trim(fgets(topic, sz, f));
    fclose(f);
    return strlen(topic) > 0;
}

int main(int argc, char** argv) {
    char* host = "52.74.88.35";
    int port = 1883;

    struct mosquitto *mosq = NULL;
    char topic[128] = "";
    char buf[BUF_SZ];

    FILE *src = stdin;

    if (!prepareTopic(topic, sizeof(topic))) {
        printf("Please put a topic string in topic.config file");
        exit(2);
    }
    debug(TAG, "Topic is %s", topic);

    // simplistic way of parsing params
    // 1st param is host
    // 2nd param is port
    
    if (argc >= 2) host = argv[1];
    if (argc >= 3) port = atoi(argv[2]);

    mosquitto_lib_init();
    mosq = mosquitto_new(TAG, 1, NULL);
    connectServer(mosq, host, port);

    while (!feof(src)) {
        if (mosquitto_loop(mosq, 2000, 10) != MOSQ_ERR_SUCCESS) {
            fgets(buf, BUF_SZ, src); // eats potential commands
            connectServer(mosq, host, port);
        }

        debug(TAG, "Waiting for input on stream");
        if (!fgets(buf, BUF_SZ, src))
            continue;

        char* str = trim(buf);
        if (!strlen(str)) continue;

        debug(TAG, "Got input [%s]", str);
        sendMessageToTopic(mosq, topic, str);
    }
    debug(TAG, "Source stream terminated");
    return 0;
}

