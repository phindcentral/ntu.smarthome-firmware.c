#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
#include "util.h"
#include "debug.h"

const char* TAG = "COMMAND_RECEIVER";

void messageReceived(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
    if (!msg->payloadlen) return;
    char* str = trim(msg->payload);
    debug(TAG, "Got data [%s]", str);
    printf("%s\n", str);
    fflush(stdout);
}

void connectServer(struct mosquitto *mosq, const char* host, int port) {
    while (mosquitto_connect(mosq, host, port, 600) != MOSQ_ERR_SUCCESS){
        debug(TAG, "Connect Failed to %s:%d", host, port);
        sleep(2);
    }
    debug(TAG, "Connected %s:%d", host, port);
}

bool prepareTopic(char* topic, int sz) {
    FILE* f = fopen("topic.config", "r");
    if (!f) return false;
    topic = trim(fgets(topic, sz, f));
    fclose(f);
    return strlen(topic) > 0;
}

int main (int argc, char** argv) {
    char* host = "52.74.88.35";
    int port = 1883;

    struct mosquitto *mosq = NULL;
    char topic[128] = "";

    if (!prepareTopic(topic, sizeof(topic))) {
        printf("Please put a topic string in topic.config file");
        exit(2);
    }
    strcat(topic, "_C");
    debug(TAG, "Topic is [%s]", topic);

    // Simplistic way of parsing arguments
    // 1st argument is host
    // 2nd argument is port

    if (argc >= 2) host = argv[1];
    if (argc >= 3) port = atoi(argv[2]);

    mosquitto_lib_init();
    mosq = mosquitto_new(TAG, 1, NULL);
    mosquitto_message_callback_set(mosq, messageReceived);

    connectServer(mosq, host, port);
    mosquitto_subscribe(mosq, NULL, topic, 0);

    while (1) {
        if (mosquitto_loop(mosq, 2000, 10) != MOSQ_ERR_SUCCESS) {
            connectServer(mosq, host, port);
            sleep(2);
        }
    }
    return 0;
}

