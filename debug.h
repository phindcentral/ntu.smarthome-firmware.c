
#ifndef _DEBUG_H_
#define _DEBUG_H_

//#define DEBUG

void debug(const char* tag, const char* fmt, ...);
void printDebugData(const char* tag, unsigned char* data, int sz);

#endif
