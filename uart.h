#ifndef _UART_H_
#define _UART_H_

#include <pthread.h>

#define PREAMBLE 0x33
#define POSTAMBLE 0x44

#define DEVICE_ID_SZ    24
#define KEY_SZ          10
#define FRAME_SZ        7

typedef enum {
    PacketTypeGetDeviceKeyReq = 0x01,
    PacketTypeGetDeviceKeyRes = 0xB1,
    PacketTypeGetDataFromDevice = 0x02,
    PacketTypeGetMultipleDataFromDevice = 0x03,
    PacketTypeSetDeviceStatus = 0x04,
    PacketTypeSetDeviceStatusBroadcast= 0x05,
    PacketTypePingDevice = 0x06,
    PacketTypeOnboarding = 0x07,
    PacketTypeTracking = 0x08,
    PacketTypeFencing = 0x09,
    PacketTypeSetPANID = 0x0A,
    PacketTypeDeviceAdoption = 0x0B
} PacketType;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} GetDeviceKeyPacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char key[KEY_SZ];
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} GetDeviceKeyResPacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char sensorId;
    unsigned char sensorType;
    union {
        int sensorDataInt;
        float sensorDataFloat;
    } __attribute__((packed));
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} GetDataFromDevicePacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char count;
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char sensorId;
    unsigned char sensorType;
    union {
        int* sensorDataInt;
        float* sensorDataFloat;
    } __attribute__((packed));
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} GetMultipleDataFromDevicePacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char sensorId;
    unsigned char sensorType;
    union {
        int sensorDataInt;
        float sensorDataFloat;
    } __attribute__((packed));
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} SetDeviceStatusPacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char sensorType;
    union {
        int sensorDataInt;
        float sensorDataFloat;
    } __attribute__((packed));
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} SetDeviceStatusBroadcastPacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} PingDevicePacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char count;
    unsigned char *deviceIdList;
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} OnboardingPacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char panId;
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} SetPANIDPacket;

typedef struct {
    unsigned char preamble;
    unsigned short type __attribute__((packed));
    unsigned char deviceId[DEVICE_ID_SZ];
    unsigned char crc;
    unsigned short invType __attribute__((packed));
    unsigned char postamble;
} DeviceAdoptionPacket;

// Function Definitions
void preparePacket(void* packet, int sz, short type);

void openUART();
FILE* getUARTInputStream();
FILE* getUARTOutputStream();

#endif

