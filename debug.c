#include <stdarg.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include "debug.h"

#ifdef DEBUG

pthread_mutex_t debugMutex = PTHREAD_MUTEX_INITIALIZER;

void debug(const char* tag, const char* fmt, ...) {
    va_list argp;
    
    pthread_mutex_lock(&debugMutex);

    va_start(argp, fmt);
    fprintf(stderr, " [DEBUG] [%s] : ", tag);
    vfprintf(stderr, fmt, argp);
    va_end(argp);

    fprintf(stderr, "\n");

    pthread_mutex_unlock(&debugMutex);
}

void printDebugData(const char* tag, unsigned char* data, int sz) {
    const int COL = 16;
    const int GRP = 8;
    char buf[1024] = "", text[1024] = "";
    int i = 0;

    sprintf(buf, "== Packet Data ====");
    for (i=0; i<sz; i++) {
        if (i%COL == 0) {
            sprintf(buf + strlen(buf), "%s\n  ", text);
            sprintf(text, "  --  ");
        }
        if (i%GRP == 0) {
            sprintf(text + strlen(text), "  ");
            sprintf(buf + strlen(buf), "  ");
        }
        char ch = data[i];
        sprintf(text + strlen(text), "%c", isprint(ch) ? ch : '.');
           
        sprintf(buf + strlen(buf), "%02x  ", data[i]);
    }
    if (i%COL != 0) sprintf(buf + strlen(buf), "%s\n", text);
    debug(tag, "%s== End Packet Data ====", buf);
}


#else

void debug(const char* tag, const char* fmt, ...) {}
void printDebugData(const char* tag, unsigned char* data, int sz) {}

#endif
