#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>

#include "server_command_handler.h"
#include "uart.h"
#include "jsmn.h"
#include "debug.h"

#define TAG "SERVER_COMMAND_HANDLER"

extern bool g_hasDeviceListing; // defined in main.c 
extern bool g_hasPANId;

char* extractStringFromToken(jsmntok_t tok, char* src, char* buf, int sz) {
    int tokSz = tok.end - tok.start;
    if (tokSz > sz) return 0;
    memcpy(buf, src + tok.start, tokSz);
    buf[tokSz] = 0;
    return buf;
}

void handleCentralServerCommand(char *data, 
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock) {

    jsmn_parser parser;
    jsmntok_t tokens[256]; // assume max 256 tokens per message

    char command[128];

    jsmn_init(&parser);
    
    if (jsmn_parse(&parser, data, strlen(data), tokens, 128) < 3) 
        return; // yeah, too small

    // parse command from command token (which we asssumed is alwasy at position 2)
    extractStringFromToken(tokens[2], data, command, sizeof(command));
    debug(TAG, "Got command %s", command);

#if DEBUG_VERBOSE
    debug(TAG, "== START PARSING ========");
    debug(TAG, "ori > %s <", data);
    debug(TAG, "ori >> %d <<", ret);

    for (int i=0; i<ret; i++) {
        jsmntok_t tok = tokens[i];
        char tmp[128];
        int sz = tok.end - tok.start;

        switch (tok.type) {
            case JSMN_STRING:
                memcpy(tmp, data + tok.start, sz);
                tmp[sz] = 0;
                debug(TAG, "  String %d %d %d [%s]", tok.start, tok.end, tok.size, tmp);
                break;
            default: 
                debug(TAG, "  Token (%d) %d %d %d", tok.type, tok.start, tok.end, tok.size);
                break;
        }
    }
    debug(TAG, "");
    debug(TAG, "== END PARSING ========");
#endif 

    if (!strcmp(command, "deviceList_res")) {
        // device list response is at tokens[4]
        int sz = tokens[4].size;
        debug(TAG, "Got %d device listing", sz);

        OnboardingPacket packet;
        preparePacket(&packet, sizeof(packet), PacketTypeOnboarding);
        packet.count = sz;
        char* deviceIdList = (char*)(calloc(sz, DEVICE_ID_SZ));

        int i;
        for (i = 0; i < sz; i++) {
            FILE *keyFile;

            char deviceId[128],
                 deviceKey[128],
                 keyFileName[1228];

            jsmntok_t idToken  = tokens[7 + (i*5)],
                      keyToken = tokens[9 + (i*5)];

            extractStringFromToken(idToken, data, deviceId, sizeof(deviceId));
            extractStringFromToken(keyToken, data, deviceKey, sizeof(deviceKey));

            sprintf(keyFileName, "./keys/%s.key", deviceId);
            
            debug(TAG, " Saving device key %s to %s", deviceKey, keyFileName);

            keyFile = fopen(keyFileName, "wb");
            fwrite(deviceKey, KEY_SZ, 1, keyFile);
            fclose(keyFile);

            // save id to onboarding packet
            memcpy(deviceIdList + i * DEVICE_ID_SZ, deviceId, DEVICE_ID_SZ);            
        }
        // send onboarding packet

				prepareCRC(&packet, sizeof(packet));			

        pthread_mutex_lock(uart_out_lock);
        fwrite(&packet, 1 + 2 + 1, 1, uart_out);
        fwrite(deviceIdList, sz * DEVICE_ID_SZ, 1, uart_out);
        fwrite(&packet.crc, 1 + 2 + 1, 1, uart_out);
        fflush(uart_out);
        pthread_mutex_unlock(uart_out_lock);

        g_hasDeviceListing = true;
    } else if (!strcmp(command, "heartbeat") ||
               !strcmp(command, "devicesList_req") ||
               !strcmp(command, "panId_req")) {
        // these are ommands sent by client to server, we ignore them
    } else if (!strcmp(command, "panId_res")) {
        char panId[128];
        
        extractStringFromToken(tokens[4], data, panId, sizeof(panId));       
        debug(TAG, "Got PAN ID %d", atoi(panId));

        // save pan ID to file
        FILE* panIdFile = fopen("./panid", "wb");
        fwrite(panId, strlen(panId), 1, panIdFile);
        fclose(panIdFile);

        // set RF Module PAN ID
        SetPANIDPacket packet;
        preparePacket(&packet, sizeof(packet), PacketTypeSetPANID);
        packet.panId = atoi(panId);
				
				prepareCRC(&packet, sizeof(packet));       
 
        pthread_mutex_lock(uart_out_lock);
        fwrite(&packet, sizeof(packet), 1, uart_out);
        fflush(uart_out);
        pthread_mutex_unlock(uart_out_lock);
        g_hasPANId = true;
    } else if (!strcmp(command, "control")) {
        char deviceId[128];
        char sensorId[128];
        char sensorType[128];
        char sensorData[128];

        extractStringFromToken(tokens[4], data, deviceId, sizeof(deviceId));
        deviceId[DEVICE_ID_SZ] = 0;
        extractStringFromToken(tokens[6], data, sensorId, sizeof(sensorId));
        extractStringFromToken(tokens[8], data, sensorType, sizeof(sensorType));
        extractStringFromToken(tokens[10], data, sensorData, sizeof(sensorData));
        debug(TAG, "Controlling %s-%d-%d with %d", 
            deviceId, atoi(sensorId), atoi(sensorType), atoi(sensorData));

        SetDeviceStatusPacket packet;
        preparePacket(&packet, sizeof(packet), PacketTypeSetDeviceStatus);
        packet.sensorId = (unsigned char)atoi(sensorId);
        packet.sensorType = (unsigned char)atoi(sensorType);
        packet.sensorDataInt = atoi(sensorData);
        memcpy(packet.deviceId, deviceId, DEVICE_ID_SZ);

				prepareCRC(&packet, sizeof(packet));		

        pthread_mutex_lock(uart_out_lock);
        fwrite(&packet, sizeof(packet), 1, uart_out);
        fflush(uart_out);
        pthread_mutex_unlock(uart_out_lock);
     } else if (!strcmp(command, "broadcast")) {
        char sensorType[128];
        char sensorData[128];

        extractStringFromToken(tokens[4], data, sensorType, sizeof(sensorType));
        extractStringFromToken(tokens[6], data, sensorData, sizeof(sensorData));

        debug(TAG, "Broadcasting to sensor type %d with %d", 
            atoi(sensorType), atoi(sensorData));

        SetDeviceStatusBroadcastPacket packet;
        preparePacket(&packet, sizeof(packet), PacketTypeSetDeviceStatusBroadcast);
        packet.sensorType = (unsigned char)atoi(sensorType);
        packet.sensorDataInt = atoi(sensorData);

				prepareCRC(&packet, sizeof(packet));

        pthread_mutex_lock(uart_out_lock);
        fwrite(&packet, sizeof(packet), 1, uart_out);
        fflush(uart_out);
        pthread_mutex_unlock(uart_out_lock);
        
    } else {
        debug(TAG, "Unknown command '%s'", command);
    }
}
