# DEBUG is a global flag to turn debug loggin on or off

DEBUG = 0
ifeq ($(DEBUG), 1)
	CFLAGS = -DDEBUG
else
	CFLAGS = -DNDEBUG
endif

MQTT_TARGETS = mqtt_command_receiver mqtt_command_sender
NETWORKING_TARGETS = $(MQTT_TARGETS)

##################################################
# Default Target

all: main $(NETWORKING_TARGETS)

##################################################
# Make Building Targetes

# generic rule to compile all .c files 

%.o: %.c
	gcc -c $< -o $@ $(CFLAGS)

# main
# This binary acts as a bridge between the UART module and the central server.
# It receives central server's command from STDIN, and send command to central
# server via STDOUT. It also opens a serial port file handle internally to read
# and write data to the UART.

main: % : %.o uart.o jsmn.o util.o server_command_handler.o debug.o 
	gcc -Wall $^ -lpthread -o $@

# mqtt_command_receiver
# This binary is responsible for listening for commands coming from central
# server, and write commands received out to STDOUT so can be piped to the main
# binary.
#

# mqtt_command_sender
# this binary is responsible for listening for commands coming on STDIN and
# send commands out to central server.

$(MQTT_TARGETS): % : %.o debug.o util.o
	gcc -Wall $^ -o $@ -lmosquitto

# mqtt_command_sender
# this binary is responsible for listening for commands coming on STDIN and
# send commands out to central server.

##################################################
# Pseudo Targets 

test: main mqtt_command_receiver mqtt_command_sender
	./mqtt_command_receiver | ./main | ./mqtt_command_sender

start: main mqtt_command_receiver mqtt_command_sender heartbeat
	./mqtt_command_receiver | ./main | ./mqtt_command_sender &

clean:
	rm -f *.o main $(NETWORKING_TARGETS)

kill:
	killall main $(NETWORKING_TARGETS)
