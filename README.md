## SmartXLab Gateway Firmware

### Introduction
----

This is the firmware programs to be run on the gateway device.

You need mosquitto library to successful build the programs. Please refer to mosquitto project page for instruction on how to install it in your testing machine. This README.md will be updated with more detail instruction when the code base is more stable.

### Installation
----

Run 
	
	make
	
To make the full program suite.


Run
	
	make kill && make test
	
To run the test.

### Program Responsibilities
----

There are 5 utitlities.

#### Main
This is the program responsible for parsing data coming from RF module UART, and send the sensor data to be logged to STDOUT, and parsing commands coming from STDIN and send the result to either disk or RF module

#### mqtt_command_sender
This is the program that take the data from STDIN (feed from Main), and wrap it in JSON payload format our Backend Server requires, and send it to the Backend Server's MQTT topic

#### mqtt_command_receiver
This is the program that monitors messages in the Backend Server's MQTT topic, and extracts commands to be processed by the gateway to the STDOUT (pipe to Main)
