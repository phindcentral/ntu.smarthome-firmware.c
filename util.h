#ifndef _UTIL_H_
#define _UTIL_H_

/**
 * Trim white space from beginning and ending of string.
 *
 * @param {String} src The string to be trimmed. The original string might be modified to
 * trim the trailing space.
 * @return {String} The string trimmed. The pointer might not be the same as the original 
 * if the original string starts with white characters.
 */
char* trim(char* src);

#endif
