#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdbool.h>
#include <pthread.h>
#include "util.h"
#include "debug.h"

#include "uart.h"
#include "server_command_handler.h"

#define DEBUG_VERBOSE  1
#define BUF_SZ 1024

#define TAG "MAIN"

FILE *stream_in, *stream_out;

bool g_hasDeviceListing = false;
bool g_hasPANId = false;

void handlerUARTCommand(unsigned char* data, int sz, 
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock);

typedef struct {
    int id;
    FILE* in_stream;
    FILE* out_stream_1;
    pthread_mutex_t* out_stream_1_lock;
    FILE* out_stream_2;
    pthread_mutex_t* out_stream_2_lock;
} MyThreadContext;

void* inputStreamReaderThreadFunction(void* param) {
    MyThreadContext* ctx = (MyThreadContext*)param;
    unsigned char buf[BUF_SZ];
    const char* tag = TAG "-T1";

    while (!feof(ctx->in_stream)) {
        char* str = fgets(buf, sizeof(buf), ctx->in_stream);

        if (!str) continue;
        str = trim(str);

        if (strlen(str) <= 0) continue;
        debug(tag, "Get Stream data [%s]", str);

        handleCentralServerCommand(str, ctx->out_stream_1, ctx->out_stream_1_lock,
            ctx->out_stream_2, ctx->out_stream_2_lock);
    }
    debug(tag, "End of Stream Data");
}

void* uartStreamReaderThreadFunction(void* param) {
    MyThreadContext* ctx = (MyThreadContext*)param;
    unsigned char buf[BUF_SZ];
    const char* tag = TAG "-T2";
    int count = 0;
    
    while (!feof(ctx->in_stream)) {
        int sz = readCompletePacketFromStream(ctx->in_stream, buf, sizeof(buf));

        if (sz < FRAME_SZ) {
            debug(tag, "Failed to get uart data, sz / error code %d", sz);
        } else {
            count ++;
            debug(tag, "Got UART packet (%d) length %d", count, sz);
            handleUARTCommand(buf, sz, ctx->out_stream_1, ctx->out_stream_1_lock,
                ctx->out_stream_2, ctx->out_stream_2_lock);
        }
#ifdef DEBUG
        sleep(1);
#endif
    }
    debug(tag, "End of UART data");
}

void* heartbeatThreadFunction(void* param) {
    MyThreadContext* ctx = (MyThreadContext*)param;
    unsigned char buf[BUF_SZ];
    const char* tag = TAG "-HB";

    while (true) {
        pthread_mutex_lock(ctx->out_stream_1_lock);
        fprintf(ctx->out_stream_1, "{\"cmd\":\"heartbeat\"}\n");
        fflush(ctx->out_stream_1);
        pthread_mutex_unlock(ctx->out_stream_1_lock);
        sleep(10);
    }
}


// returns number of bytes read. -1 if buffer too small, -2 if stream ended prematurely
int readCompletePacketFromStream(FILE* stream, char* buf, int sz) {
    char* f = buf;
    int state = 0; // 0 => waiting for PREAMBLE, 1 => waiting for POSTAMBLE
    while (!feof(stream)) {
        char ch;
        fread(&ch, sizeof(ch), 1, stream); // read 1 byte to ch
        // process state machine
        if (state == 0) { // waiting for PREAMBLE
            if (ch == PREAMBLE) { // found
                *(f++) = ch; // save byte and increment
                state = 1; // waiting for POSTAMBLE
            } else {
								  debug("PREADER", "Invalid char %c [%d]", isprint(ch)?ch:'?', ch);
						}
        } else if (state == 1) { // waiting for POSTAMBLE
            if (f - buf > sz) return -1; // error condition, buf too small
            *(f++) = ch; // save byte and increment
            if (ch == POSTAMBLE) { // found
                return f - buf; // return count
            }
        }
    }
    if (state == 0) return 0; // was still waiting for PREAMBLE
    return -2; // error codition, premature end of stream
}

int main(int argc, char** argv) {
    pthread_t thread1, thread2, thread3;

#ifdef DEBUG
    bool isDebugDataEnded = false;
#endif

    sleep(1);

    openUART();
    FILE* uart_in = getUARTInputStream();
    FILE *uart_out = getUARTOutputStream();
    
    stream_in = stdin;
    stream_out = stdout;

    pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

    MyThreadContext ctx1;
    memset(&ctx1, 0, sizeof(ctx1));
    ctx1.id = 1;
    ctx1.in_stream = stream_in;
    ctx1.out_stream_1 = stream_out;
    ctx1.out_stream_2 = uart_out;
    ctx1.out_stream_1_lock = &mutex1;
    ctx1.out_stream_2_lock = &mutex2;
  	pthread_create(&thread1, NULL, inputStreamReaderThreadFunction, (void*)&ctx1);

    MyThreadContext ctx2;
    memset(&ctx2, 0, sizeof(ctx2));
    ctx2.id = 2;
    ctx2.in_stream = uart_in;
    ctx2.out_stream_1 = stream_out;
    ctx2.out_stream_2 = uart_out;
    ctx2.out_stream_1_lock = &mutex1;
    ctx2.out_stream_2_lock = &mutex2;
    pthread_create(&thread2, NULL, uartStreamReaderThreadFunction, (void*)&ctx2);

    MyThreadContext ctx3;
    memset(&ctx3, 0, sizeof(ctx3));
    ctx3.id = 3;
    ctx3.in_stream = uart_in;
    ctx3.out_stream_1 = stream_out;
    ctx3.out_stream_2 = uart_out;
    ctx3.out_stream_1_lock = &mutex1;
    ctx3.out_stream_2_lock = &mutex2;
    pthread_create(&thread3, NULL, heartbeatThreadFunction, (void*)&ctx3);

    debug(TAG, "Sending Device List Request");
    pthread_mutex_lock(&mutex1);
    fprintf(stream_out, "{\"cmd\":\"devicesList_req\"}\n");
    fprintf(stream_out, "{\"cmd\":\"panId_req\"}\n");
    fflush(stream_out);
    pthread_mutex_unlock(&mutex1);

   	pthread_join(thread1, NULL);
    debug(TAG, "Thread1 Joined");
    pthread_join(thread2, NULL);
    debug(TAG, "Thread2 Joined");

    return 0;
}
