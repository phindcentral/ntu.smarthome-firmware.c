#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <termios.h>
#include <stdbool.h>
#include <fcntl.h>
#include "uart.h"
#include "debug.h"

#define TAG "UART"

#define USE_MOCKUP 1

#if USE_MOCKUP
    #define MOCKUP_DEV "553771888507c0d971bc92b0"
#else
    #define SERIAL_PORT_DEV "/dev/ttyS0"
#endif

#define POLYNOMINAL 0x1D

FILE* g_uartInputStream = NULL; 
FILE* g_uartOutputStream = NULL;

bool set_interface_attribs(int fd, int speed, int parity) {
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if (tcgetattr(fd, &tty) != 0) {
        debug(TAG, "Error %d from tcgetattr", errno);
        return false;
    }

    tty.c_cflag = speed | CREAD | CLOCAL | CS8;
    tty.c_iflag = BRKINT | IGNPAR | IXON;
    tty.c_oflag = 0;
    tty.c_lflag = IEXTEN | ECHOE | ECHOK | ECHOCTL | ECHOKE;
    tty.c_cc[VINTR] = 003;
    tty.c_cc[VQUIT] = 034;
    tty.c_cc[VERASE] = 0177;
    tty.c_cc[VKILL] = 025;
    tty.c_cc[VEOF] = 004;
    tty.c_cc[VSTART] = 021;
    tty.c_cc[VSTOP] = 023;
    tty.c_cc[VSUSP] = 032;
    tty.c_cc[VREPRINT] = 0;
    tty.c_cc[VWERASE] = 027;
    tty.c_cc[VLNEXT] = 026;
    tty.c_cc[VMIN] = 100;
    tty.c_cc[VTIME] = 2;
    
    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        debug(TAG, "Error %d from tcsetattr", errno);
        return false;
    }
    
    return true;
}

void openUART() {
#if USE_MOCKUP
    g_uartInputStream = fopen("sample.dat", "rb");
    g_uartOutputStream = fopen("rfInput", "wb");
#else
    // open serial port read/write
    debug(TAG, "Opening serial port " SERIAL_PORT_DEV);
    int fd = open(SERIAL_PORT_DEV, O_RDWR | O_NOCTTY | O_SYNC);
int i;
char buffer[16];
    if (fd < 0) {
        debug(TAG, "Failed to open serial port, %d, %s",  errno, strerror(errno));
        exit(1);
    }
    
    // set spped to 115,200 bps, 8n1, (no parity)
    if (!set_interface_attribs(fd, B115200, 0)) {
        exit(1);
    }
    fcntl(fd, F_SETFL, 0);
    g_uartInputStream = fdopen(fd, "w+");
    g_uartOutputStream = g_uartInputStream;
#endif
}

FILE* getUARTInputStream() {
    return g_uartInputStream;
}

FILE* getUARTOutputStream() {
    return g_uartOutputStream;
}

void getPacketInfo(void* packet, int sz, short* type, short* invType, unsigned char* crc);
void preparePacket(void* packet, int sz, short type);
void prepareCRC(void* packet, int sz);
char calculateCRC(void* packet, int sz);

// packet handler
void handleGetDeviceKeyReqPacket(GetDeviceKeyPacket packet, 
    FILE* stream_out, pthread_mutex_t* stream_out_lock, 
    FILE* uart_out, pthread_mutex_t* uart_out_lock);
void handleGetDataFromDevicePacket(GetDataFromDevicePacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock);
void handleGetMultipleDataFromDevicePacket(GetMultipleDataFromDevicePacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock);
void handleDeviceAdoptionPacket(DeviceAdoptionPacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock);

void handleUARTCommand(unsigned char* data, int sz, 
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock) {

    int i;
    char deviceKeyFileName[40];
    FILE *deviceKeyFile;
   
    printDebugData(TAG, data, sz);


    if (sz < FRAME_SZ)  {
        debug(TAG, "Packet is only %d bytes long, dropping", sz); 
        return;
    }

    short type, invType;
    unsigned char crc;
		unsigned char calculate_crc = calculateCRC(data, sz);
    getPacketInfo(data, sz, &type, &invType, &crc);

    if (type != ~invType) {
        debug(TAG, "Packet has wrong inverse type 0x%04x, expecting 0x%04x from 0x%04x",
            (int)invType, (int)(~type), (int)type);
        return;
    }

		debug(TAG, "calculateCRC= %02X", calculate_crc);
	/*	if (crc != calculateCRC) {
				debug(TAG, "Packet has wrong CRC");
				return;
		}*/

    switch (type) {
        case PacketTypeGetDeviceKeyReq: {
            int expSz = FRAME_SZ + DEVICE_ID_SZ;
            if (sz != FRAME_SZ + DEVICE_ID_SZ) {
                debug(TAG, "Get Device Key Request should only be %d bytes long, got %d", sz, expSz);
                return;
            }
            GetDeviceKeyPacket packet;
            memcpy(&packet, data, sizeof(packet));
            handleGetDeviceKeyReqPacket(packet, stream_out, stream_out_lock, uart_out, uart_out_lock);
        } break;
        case PacketTypeGetDataFromDevice: {
            int expSz = FRAME_SZ + DEVICE_ID_SZ + 1 + 1 + 4;
            if (sz != expSz) {
                debug(TAG, "Get Data From Device should only be %d bytes long, got %d", sz, expSz);
                return;
            }
            GetDataFromDevicePacket packet;
            memcpy(&packet, data, sizeof(packet));
            handleGetDataFromDevicePacket(packet, stream_out, stream_out_lock, uart_out, uart_out_lock);
        } break;
        case PacketTypeGetMultipleDataFromDevice: {
            char cnt = data[3];
            int expSz = FRAME_SZ + 1 + DEVICE_ID_SZ + 1 + 1 + sizeof(int) * cnt;
            if (sz != expSz) {
                debug(TAG, "Get Multiple Data From Device should be %d bytes long, got %d", sz, expSz);
                return;
            }
            // allocate memory for sensor data fields
            int* sensorData = (int*)(malloc(sizeof(int) * cnt));

            GetMultipleDataFromDevicePacket packet;
            memcpy(&packet, data, 1 + 2 + 1 + DEVICE_ID_SZ + 1 + 1);
            memcpy(sensorData, data + 1 + 2 + 1 + DEVICE_ID_SZ + 1 + 1, sizeof(int) * cnt);
            packet.sensorDataInt = sensorData;
            memcpy(&packet.crc, data + sz-4, 1 + 2 + 1);

            handleGetMultipleDataFromDevicePacket(packet, stream_out, stream_out_lock, uart_out, uart_out_lock);

            free(sensorData);
        } break;
        case PacketTypeDeviceAdoption: {
            int expSz = FRAME_SZ + DEVICE_ID_SZ;
            if (sz != expSz) {
                debug(TAG, "Devie Adoption Packet should only be %d bytes long, got %d", sz, expSz);
                return;
            }
            DeviceAdoptionPacket packet;
            memcpy(&packet, data, sz);
            handleDeviceAdoptionPacket(packet, stream_out, stream_out_lock, uart_out, uart_out_lock);
        } break;
        default: {
            debug(TAG, "Unknown command type %d", type);
        }
    }
  
    pthread_mutex_lock(uart_out_lock);
    fflush(uart_out);
    pthread_mutex_unlock(uart_out_lock);

    pthread_mutex_lock(stream_out_lock);
    fflush(stream_out);
    pthread_mutex_unlock(stream_out_lock);
}

void getPacketInfo(void* packet, int sz, short* type, short* invType, unsigned char* crc)
{
		*type      = *((short*)(packet+1));
    *invType   = *((short*)(packet+sz-3));
    *crc       = *((char*)(packet+sz-4));
}

void preparePacket(void* packet, int sz, short type) {
    memset(packet, 0, sz); // clearout
    *((char*)(packet + 0)) = PREAMBLE;
    *((short*)(packet + 1)) = type;
    *((unsigned char*)(packet + sz-4)) = calculateCRC(packet, sz); // crc
    *((short*)(packet + sz-3)) = ~type;
    *((char* )(packet + sz-1)) = POSTAMBLE;
}

void prepareCRC(void* packet, int sz) {
    *((unsigned char*)(packet + sz-4)) = calculateCRC(packet, sz);
}

char calculateCRC(void* packet, int sz) {
    int i, j;
    unsigned char CRC = 0;
    char* payload;
    int payload_sz = sz - 7;

    payload = (char*)malloc(sizeof(char)*payload_sz);
    memcpy(payload, ((char*)packet)+3, payload_sz);

    for (i = 0; i < payload_sz; i++) {
      CRC ^= payload[i];
      for (j = 0; j < 8; j++) {
        if(CRC & 0x80) {
          CRC = (CRC << 1) ^ POLYNOMINAL;
        } else {
          CRC <<= 1;
        }
      }
      CRC &= 0xff;
    }
    free(payload);

    return CRC;
}

void handleGetDeviceKeyReqPacket(GetDeviceKeyPacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock) {
    
    debug(TAG, "Handle Get device Key Request Packet");
    GetDeviceKeyResPacket responsePacket;
    preparePacket(&responsePacket, sizeof(GetDeviceKeyResPacket), PacketTypeGetDeviceKeyRes);

    debug(TAG, " Requesting key for device id %s", packet.deviceId);

#ifdef MOCKUP_DEV
    debug(TAG, " Override with devID %s", MOCKUP_DEV);
    memcpy(packet.deviceId, MOCKUP_DEV, DEVICE_ID_SZ); 
#endif

    char deviceKeyFileName[128];
    sprintf(deviceKeyFileName, "./keys/%s.key", packet.deviceId);
    FILE* deviceKeyFile = fopen(deviceKeyFileName, "rb");
    if (deviceKeyFile) {
        char buf[128];
        memcpy(responsePacket.deviceId, packet.deviceId, sizeof(packet.deviceId));
        fread(buf, sizeof(buf), 1, deviceKeyFile);
        fclose(deviceKeyFile);
        debug (TAG, "Key found %s", buf);
        memcpy(responsePacket.key, buf, KEY_SZ);
    } else {
        debug(TAG, "Key not found");
    }
    debug(TAG, " Response Packet:");
    printDebugData(TAG, (unsigned char*)&responsePacket, sizeof(responsePacket));
        
    pthread_mutex_lock(uart_out_lock);
    fwrite(&responsePacket, sizeof(responsePacket), 1, uart_out);
    pthread_mutex_unlock(uart_out_lock);
}

void handleGetDataFromDevicePacket(GetDataFromDevicePacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock) {

    debug(TAG, "Handle Get Data From Device Packet");
    debug(TAG, " Got 1 data points from sensor id %d, type %d", 
        packet.sensorId, packet.sensorType);
    debug(TAG, "   #%d: %d", 0, packet.sensorDataInt);

    char deviceId[128];
    memcpy(deviceId, packet.deviceId, DEVICE_ID_SZ);
#ifdef MOCKUP_DEV
    memcpy(deviceId, MOCKUP_DEV, DEVICE_ID_SZ); 
#endif
    deviceId[DEVICE_ID_SZ] = 0; // make C-string

    pthread_mutex_lock(stream_out_lock);
    fprintf(stream_out, "{\"cmd\":\"log\",\"deviceId\":\"%s\",\"sensorId\":%d,\"sensorType\":%d,\"data\":%d}\n",
        deviceId, packet.sensorId, packet.sensorType, packet.sensorDataInt);
    pthread_mutex_unlock(stream_out_lock);
}

void handleGetMultipleDataFromDevicePacket(GetMultipleDataFromDevicePacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock) {

    debug(TAG, "Handle Get Multiple Data From Device Packet");    
    debug(TAG, " Got %d data points from sensor id %d, type %d", 
        packet.count, packet.sensorId, packet.sensorType);
    
    char deviceId[128];
    memcpy(deviceId, packet.deviceId, DEVICE_ID_SZ);
#ifdef MOCKUP_DEV
    memcpy(deviceId, MOCKUP_DEV, DEVICE_ID_SZ); 
#endif
    deviceId[DEVICE_ID_SZ] = 0; // make C-string

    int i = 0;
    for (i=0; i<packet.count; i++) {
        int data = *(packet.sensorDataInt+i);
        debug(TAG, "   #%d: %d", i, data);

        pthread_mutex_lock(stream_out_lock);
        fprintf(stream_out, "{\"cmd\":\"log\",\"deviceId\":\"%s\",\"sensorId\":%d,\"sensorType\":%d,\"data\":%d}\n",
            deviceId, packet.sensorId, packet.sensorType, data);
        pthread_mutex_unlock(stream_out_lock);
    }
}

void handleDeviceAdoptionPacket(DeviceAdoptionPacket packet,
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock) {

    debug(TAG, "Handle Device Adoption Packet");

    char deviceId[128];
    memcpy(deviceId, packet.deviceId, DEVICE_ID_SZ);
#ifdef MOCKUP_DEV
    memcpy(deviceId, MOCKUP_DEV, DEVICE_ID_SZ); 
#endif
    deviceId[DEVICE_ID_SZ] = 0; // make C-string

    debug(TAG, "  Reporting device ID [%s] onboarded", deviceId);
    
    pthread_mutex_lock(stream_out_lock);
    fprintf(stream_out, "{\"cmd\":\"deviceAdopted_notif\",\"deviceId\":\"%s\"}\n", deviceId);
    pthread_mutex_unlock(stream_out_lock);
}

