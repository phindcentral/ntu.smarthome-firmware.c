#include <string.h>
#include "util.h"

char* trim(char* src) {
    char* f = src;
    char* res = src;
    
    // find the 1st non-space char
    while (*f) {
        if (!isspace(*f)) {
            res = f; // reset result starting pointer
            break;
        }
        f++;
    }

    // if we've readed the end, we are done
    if (*f == 0) return f;

    // walk backword to find the 1st non-space char
    f = src + strlen(src) - 1;
    while (f > res) {
        if (!isspace(*f)) {
            *(f+1) = 0;
            break;
        }
        f--;
    }

    // res has the right trimmed string
    return res;
}

