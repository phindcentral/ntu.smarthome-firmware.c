#ifndef _SERVER_COMMAND_HANDLER_H_
#define _SERVER_COMMAND_HANDLER_H_

void handleCentralServerCommand(char *data, 
    FILE* stream_out, pthread_mutex_t* stream_out_lock,
    FILE* uart_out, pthread_mutex_t* uart_out_lock);

#endif
